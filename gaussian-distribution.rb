#Basic file

require 'gosu'
require 'distribution'

WINDOW_HEIGHT = 320
WINDOW_WIDTH = 568

class Window < Gosu::Window
  def initialize
    super WINDOW_WIDTH, WINDOW_HEIGHT, false, 1
    self.caption = "Gaussian Distribution"
    @distribution = Distribution::Normal.rng(WINDOW_WIDTH / 2, WINDOW_WIDTH / 2)
    @positions = Array.new
  end

  def update
    @positions << @distribution.call
  end

  def draw
    Gosu.draw_rect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, Gosu::Color.new(0xffffffff))
    @positions.each do |position|
      Gosu.draw_rect(position, 0, 1, WINDOW_HEIGHT, Gosu::Color.new(0x01000000))
    end
  end
end

window = Window.new
window.show